import React from "react";
import "./More.css";
/* import '../../CV/CV' */

const More = ({ languages, habilities, volunteer }) => {
  return (
    <>
    <div className="hero">
      <div className="c-more">
      <h3> 📕 Idiomas </h3>
        {languages.map((item) => {
          return (
           
            <div key={JSON.stringify(item)}>
           
              <h4 className="card">{item.language}</h4>
              <p>{item.wrlevel}</p>
              <p>{item.splevel}</p>
             
            </div>
          );
        })}
      </div>
      <div className="c-more">
      <h3> 🦾 Habilidades</h3>

        {habilities.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
            
              <p > {item}</p>
             
             
            </div>
          );
        })}
      </div>
      <div className="c-more">
        {volunteer.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
            <h3>🦾 Voluntariado</h3>
              <p >{item.name}</p>
              <p>{item.where}</p>
              <p>{item.description}</p>
             
            </div>
          );
        })}
      </div>
      </div>
    </>
  );
};

export default More;  