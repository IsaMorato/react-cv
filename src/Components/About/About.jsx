import React from "react";
import "./About.css";
import "../Hero/Hero"

const About = ({ hero }) => {
  return (
      <div className= "hero">
      <div className="c-about">
        {hero.aboutMe.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <p>{item.info}</p>
            
            </div>
          );
        })}
      </div>
      </div>
  );
};

export default About;