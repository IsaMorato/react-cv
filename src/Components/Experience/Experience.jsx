import React from "react";
import "./Experience.css";

const Experience = ({ experience }) => {
  return (
    <div  className = "hero">
      <div className="experience-card">
        {experience.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <h3 className="card"> 💾 {item.name}</h3>
              <p>{item.date}</p>
              <p>{item.where}</p>
              <p>{item.description}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Experience;