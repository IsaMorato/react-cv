import React from "react";
import "./Education.css";

const Education = ({ education }) => {
  return (
    <div className="hero">
      <div className="education-card">
        {education.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <h3 className="card"> 📕 {item.name}</h3>
              <p>{item.where}</p>
              <p>{item.date}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Education;