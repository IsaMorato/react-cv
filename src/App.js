import { useState } from "react";
import "./App.css";
import { CV } from "./CV/CV";
import Hero from "./Components/Hero/Hero";
import Education from "./Components/Education/Education";
import Experience from "./Components/Experience/Experience";
import About from "./Components/About/About";
import More from "./Components/More/More";

const { hero, education, experience , languages, habilities, volunteer} =
  CV;

function App() {
  const [showEducation, setShowEducation] = useState(true);
  return (
    <div className="App">
      <Hero hero={hero} />
     <About hero={hero} />
    

      <button
        className="custom-btn btn-4"
        onClick={() => setShowEducation(true)}
      >
        Estudios
      </button>
      <button
        className="custom-btn btn-4"
        onClick={() => setShowEducation(false)}
      >
        Experiencia
      </button>
      <div>
        {showEducation ? (
          <Education education={education} />
        ) : (
          <Experience experience={experience} />
        )}
      </div>
      <More
        languages={languages}
        habilities={habilities}
        volunteer={volunteer}
	      />

    
    </div>
  );
}

export default App;
